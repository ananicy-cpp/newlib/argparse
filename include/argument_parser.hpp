#pragma once

#include <atomic>
#include <concepts>
#include <format>
#include <map>
#include <optional>
// #include <spdlog/spdlog.h>
#include <string>
#include <typeindex>
#include <unordered_map>
#include <utility>
#include <vector>
#include <iostream>
#include <filesystem>
#include <cxxabi.h>


namespace argparse {

    static inline std::string demangle_symbol(std::type_index type_index) {
      std::unique_ptr<char, decltype(&free)> demangled_name(abi::__cxa_demangle(type_index.name(),
                                                               nullptr, nullptr, nullptr),
                                                            &free);

      return { demangled_name.get() };
    }

    class argparse_error : std::exception {
    private:
        std::optional<std::string> error_message = {};
        static const char* default_error_message;

        static bool print_message;

    public:
        static void disable_message(bool disable = true) {
          print_message = !disable;
        }

        argparse_error() = default;
        explicit(false) argparse_error(std::string message)
                : error_message(message) {
          std::cerr << std::format("{}\n", message);
        };
        explicit(false) operator bool() const { return error_message.has_value(); }
        [[nodiscard]] const char* what() {
          return error_message.has_value() ? error_message.value().c_str() : default_error_message ;
        }
    };

    class argument_value {
    private:
        std::optional<std::string> m_value;
        std::type_index            m_type;

        void dynamic_type_check(const std::type_index& type1, const std::type_index& type2) const {
          if (type1.hash_code() != type2.hash_code()) {
            throw argparse_error(std::format("Can't convert from {} to {}",
                                             demangle_symbol(type1), demangle_symbol(type2)));
          }
        }

    public:
        argument_value(std::optional<std::string> value, std::type_index type)
                : m_value(std::move(value)), m_type(type) {}

        explicit(false) operator std::optional<std::string>() const { return m_value; }

        template <typename T>
        [[nodiscard]] std::optional<T> value() const = delete;

        template <std::integral T>
        [[nodiscard]] std::optional<T> value() const {
          dynamic_type_check(m_type, typeid(T {}));

          if (!m_value.has_value())
            return {};

          if constexpr (std::signed_integral<T>) {
            return std::stoll(m_value.value());
          } else {
            return std::stoull(m_value.value());
          }
        }

        template <std::convertible_to<std::filesystem::path> T>
        [[nodiscard]] std::optional<T> value() const {
          if (!m_value.has_value())
            return {};

          return std::filesystem::path(m_value.value());
        }

        explicit(false) operator bool() const;
    };

    class argument {
        static std::atomic<unsigned> _id;

        std::string                m_name{};
        std::vector<char>          m_short_names{};
        std::vector<std::string>   m_long_names{};
        std::string                m_description{};
        std::optional<std::string> m_value{};
        std::type_index            m_value_type = typeid(bool);
        bool                       m_is_positional = false;
        bool                       m_is_flag = true;
        bool                       m_is_optional = false;

    public:
        const unsigned id;

        argument() : id(_id++) { m_name = "<undefined>"; };
        explicit(false) argument(const std::string &name);

        argument &positional(bool enable = true);
        argument &description(const std::string &description);
        argument &type(const std::type_info &type);
        argument &name(const std::string &name);
        argument &meta_name(const std::string &name);
        argument &required(bool enable = true);
        argument &optional(bool enable = false);

        void set_value(const std::string &value);

        [[nodiscard]] std::vector<std::string>   get_long_names() const;
        [[nodiscard]] std::vector<char>          get_short_names() const;
        [[nodiscard]] std::string                get_name() const;
        [[nodiscard]] std::type_index            get_type() const;
        [[nodiscard]] std::optional<std::string> get_value() const;
        [[nodiscard]] std::optional<std::string> get_description() const;

        [[nodiscard]] bool is_positional() const;
        [[nodiscard]] bool is_flag() const;
        [[nodiscard]] bool is_defined() const;
        [[nodiscard]] bool is_optional() const;

        explicit(false) operator std::string() const;

        friend bool operator==(const argument &arg, const std::string &name) {
          return arg.m_name == name;
        }
    };

    class argument_parser {
        std::map<unsigned, argument> m_args{};
        std::string                  m_program_name {};

    public:
        argument_parser(std::string program_name = "");
        argparse_error parse_args(const std::vector<std::string> &args);
        argparse_error parse_args(int argc, const char **argv);

        argument &add_argument();
        argument &add_argument(const argument &argument);
        argument &add_argument(std::string name);

        std::optional<argument> get_arg(const std::string &name);
        std::string             get_program_name();

        void print_help();

        argument_value operator[](const std::string &name);

        template <typename T>
        std::optional<T> operator[](const std::string &name) = delete;
    };
} // namespace argparse
