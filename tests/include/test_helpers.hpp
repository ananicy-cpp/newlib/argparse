#pragma once

#include <string_view>
#include <string>

struct source_location {
private:
    int m_line;
    const char* m_file;

public:
    constexpr source_location(int line, const char* file)
    : m_line(line), m_file(file) {}

    [[nodiscard]] const char* file_name() const { return m_file; }
    [[nodiscard]] int line() const { return m_line; }
};

#define CURRENT_SOURCE_LOCATION ::source_location(__LINE__, __FILE__)

constexpr static inline void dynamic_assert(bool assertion,
                                            const std::string_view& message,
                                            const source_location& location) {
  if (!assertion) {
    throw std::runtime_error(
            std::format("Assertion failed: {} in {}:{}", message.data(),
                        location.file_name(), location.line())
    );
  }
}
