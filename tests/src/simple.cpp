#include <format>
#include <iostream>
#include <string>
#include <cstdlib>
#include <optional>
#include <filesystem>
#include <argument_parser.hpp>

#include "test_helpers.hpp"


int main(int argc, const char** argv) {

  argparse::argument_parser parser("simple_test");

  parser.add_argument()
          .name("--long-arg")
          .optional(true)
          .type(typeid(std::string))
          .description("This is a long argument");

  parser.add_argument()
          .meta_name("short-arg")
          .name("-s")
          .type(typeid(int))
          .description("Short, type-checked argument");

  parser.add_argument()
          .name("INPUT_FILE")
          .positional()
          .type(typeid(std::filesystem::path))
          .description("Input file");

  parser.print_help();

  try {
    parser.parse_args({"program_name", "-s", "42", "--long-arg", "tomatoes"});
  } catch (const argparse::argparse_error& e) {
    // Expected to fail
  }

  parser.parse_args({"program_name", "-s", "42", "--long-arg", "tomatoes", "paf.txt"});

  std::optional<std::string> long_arg_value { parser["long-arg"] };
  std::optional<int> short_arg_value { parser["short-arg"].value<int>() };

  try {
    std::optional<int> input_file_value{parser["INPUT_FILE"].value<int>()};
  } catch (const argparse::argparse_error& e) {
    // Expected to fail
  }

  std::optional<std::filesystem::path> input_file_value{parser["INPUT_FILE"].value<std::filesystem::path>()};

  std::cout << std::format("Long arg: {}, short arg: {}, input_file: {}\n",
                           long_arg_value.value_or("<Undefined>"),
                           short_arg_value.value_or(-1),
                           input_file_value.value_or("/"));

  dynamic_assert(long_arg_value.value_or("") == "tomatoes",
                 "Wrong value for long-arg", CURRENT_SOURCE_LOCATION);
  dynamic_assert(short_arg_value.value_or(0) == 42,
                 "Wrong value for short-arg", CURRENT_SOURCE_LOCATION);
  dynamic_assert(input_file_value.value_or("/") == "paf.txt",
                 "Wrong value for input_file", CURRENT_SOURCE_LOCATION);


  if (argc > 1) {
    parser.parse_args(argc, argv);

    std::cout << std::format("Long arg: {}, short arg: {}, input_file: {}\n",
                             long_arg_value.value_or("<Undefined>"),
                             short_arg_value.value_or(-1),
                             input_file_value.value_or("/"));
  }

  return EXIT_SUCCESS;
}