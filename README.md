# newlib::argparse

This is an easy-to-use, functional-style argument parsing library for your C++ projects.
No longer do you need to use `getopt` or anything equivalent.

This is pure, C++ 20 code using as little dependencies as possible
(only `stl_polyfill::format` is needed, and that is because no C++ compiler support `std::format` yet)

## Usage

### In your `CMakeLists.txt`
```cmake
find_package(NewlibArgparse 0.1)
if(NOT NewlibArgparse_FOUND)
    include(FetchContent)
    FetchContent_Declare(newlib_argparse
    GIT_REPOSITORY https://gitlab.com/ananicy-cpp/newlib/argparse.git
    GIT_TAG <commit SHA1 or tag>)
    
    FetchContent_MakeAvailable(newlib_argparse)
endif()

target_link_libraries(<TARGET_NAME> PRIVATE newlib::argparse)
```

### In your code

```c++
#include <iostream>
#include <cstdlib>
#include <argument_parser.hpp>

int main(int argc, char** argv) {
  argparse::argument_parser parser("my_program");
  
  parser.add_argument()
          .meta_name("help")
          .name("-h")
          .name("--help")
          .optional(true)
          .description("Display help message.");
  
  parser.parse_args(argc, argv);
  
  if(parser["help"]) {
    parser.print_help();
  }
  
  return EXIT_SUCCESS;
}
```

You can look at the `tests/src/simple.cpp` test file to understand more
until we write an actual documentation.